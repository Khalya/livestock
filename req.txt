asgiref==3.5.0
certifi==2021.10.8
charset-normalizer==2.0.12
Django==4.0.2
django-filter==21.1
djangorestframework==3.13.1
djangorestframework-simplejwt==5.0.0
idna==3.3
importlib-metadata==4.11.1
Markdown==3.3.6
pydantic==1.9.0
PyJWT==2.3.0
pytz==2021.3
requests==2.27.1
sqlparse==0.4.2
typing-extensions==4.1.1
tzdata==2021.5
urllib3==1.26.8
zipp==3.7.0
