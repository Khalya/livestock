from rest_framework import status
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from core.helpers import send_message_reset_password
from user.models import User, Owner
from user.serializers import UserModelSerializer, OwnerModelSerializer


class OwnerModelViewSet(ModelViewSet):
    queryset = Owner.objects.all()
    serializer_class = OwnerModelSerializer
    permission_classes = [AllowAny]

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response({'inspections': serializer.data})


class UserModelViewSet(ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserModelSerializer
    permission_classes = [AllowAny]

    def create(self, request, *args, **kwargs):

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        instance = serializer.save()
        instance.set_password(request.data.get('password'))
        instance.is_active = True
        instance.save()
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    @action(methods=['post'], detail=False)
    def check_code(self, request, pk=None):
        phone_number = request.data.get('phone_number')
        code = request.data.get('code')

        user = User.objects.get(username=phone_number)
        try:
            if str(user.verification_code) == str(code):
                return Response({'verification_code': user.verification_code}, status=200)
            else:
                raise ValidationError('Код неверный !')
        except User.DoesNotExist:
            raise ValidationError('Пользователь не найден')

    @action(methods=['post'], detail=False)
    def reset_password(self, request, pk=None):
        phone_number = request.data.get('phone_number')
        password = request.data.get('password')
        try:
            user = User.objects.get(username=phone_number)
            user.set_password(password)
            user.save()
            serializer = self.get_serializer(user, many=False)
            return Response({'user': serializer.data})
        except User.DoesNotExist:
            raise ValidationError('Пользователь не найден')

    @action(methods=['post'], detail=False)
    def send_sms(self, request, pk=None):
        phone_number = request.data.get('phone_number')
        try:
            user = User.objects.get(username=phone_number)
            send_message_reset_password(phone_number)
            return Response({'verification_code': user.verification_code})
        except User.DoesNotExist:
            raise ValidationError('Пользователь не найден')

    @action(methods=['post'], detail=False)
    def change_user_type(self, request, pk=None):
        user_type = request.data.get('user_type')
        owner = request.data.get('owner')
        username = request.data.get('username')
        try:
            user = User.objects.get(username=username)
            user.user_type = user_type
            user.owner_id = owner
            user.save()
        except User.DoesNotExist:
            raise ValidationError('Пользователь не найден')
        serializer = self.get_serializer(user, many=False)
        return Response({'user': serializer.data})

    @action(methods=["post"], detail=False)
    def change_username(self, request, pk=None):
        username = request.data.get('username', None)
        if not username:
            raise ValidationError('Укажите номер !')
        user = request.user
        user.username = username
        user.save()
        serializer = self.get_serializer(user, many=False)
        return Response({
            'user': serializer.data
        }, status=200)
