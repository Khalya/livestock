from rest_framework import routers

from user.api_view import UserModelViewSet, OwnerModelViewSet

router = routers.SimpleRouter()

router.register('users', UserModelViewSet)
router.register('owners', OwnerModelViewSet)
