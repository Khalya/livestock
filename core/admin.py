from django.contrib import admin
from .models import Beast, Lost, Inspection, Death, CauseDeath, CallDoctor, BeastView, Laboratory


@admin.register(Beast)
@admin.register(Lost)
@admin.register(Inspection)
@admin.register(Death)
@admin.register(CauseDeath)
@admin.register(CallDoctor)
@admin.register(BeastView)
@admin.register(Laboratory)
class AdminModel(admin.ModelAdmin):
    pass
