import random

import requests
from rest_framework.exceptions import ValidationError

from user.models import User


def send_message(phone_number):
    try:
        user = User.objects.get(username=phone_number)
        if not user.verification_code:
            code = random.randint(1000, 9999)
            user.verification_code = code
            user.save()
            code = random.randint(1000, 9999)
            params = {
                'recipient': phone_number.replace('+', '').replace(' ', '').replace('-', ''),
                'text': f'Ваш код для входа в систему livestock {code}',
                'apiKey': 'kz260be53e2aca4220c5e8815bcc06d3b4749966f3b82d75e66a49bb02ebd6e713122c'
            }
            requests.get("https://api.mobizon.kz/service/Message/SendSmsMessage", params=params)

    except User.DoesNotExist:
        raise ValidationError('Пользователь не найден')


def send_message_reset_password(phone_number):
    try:
        user = User.objects.get(username=phone_number)
        code = random.randint(1000, 9999)
        params = {
            'recipient': str(phone_number.replace('+', '').replace(' ', '').replace('-', '')),
            'text': f'Ваш код для входа в систему livestock {code}',
            'apiKey': 'kz260be53e2aca4220c5e8815bcc06d3b4749966f3b82d75e66a49bb02ebd6e713122c',

        }
        resp = requests.get("https://api.mobizon.kz/service/Message/SendSmsMessage", params=params)
        print(resp.text)
        user.verification_code = code
        user.save()
    except User.DoesNotExist:
        raise ValidationError('Пользователь не найден')