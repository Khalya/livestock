from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from core.models import Beast, Laboratory, Inspection, BeastView, CallDoctor, Lost, Death, CauseDeath
from core.serializers import BeastModelSerializer, LaboratoryModelSerializer, InspectionModelSerializer, \
    BeastViewModelSerializer, LostModelSerializer, CallDoctorModelSerializer, DeathModelSerializer, \
    CauseDeathModelSerializer
from user.models import User


class BeastModelViewSet(ModelViewSet):
    queryset = Beast.objects.all()
    serializer_class = BeastModelSerializer
    permission_classes = [AllowAny]

    def list(self, request, *args, **kwargs):
        number = request.GET.get('number', None)
        user = request.user
        if number:
            try:
                if not user.is_authenticated:
                    queryset = self.filter_queryset(self.get_queryset().get(number=number))
                else:
                    queryset = self.filter_queryset(self.get_queryset().get(number=number, owner_id=user.owner.id))
                serializer = self.get_serializer(queryset, many=False)
                return Response({'beast': serializer.data})
            except Beast.DoesNotExist:
                raise ValidationError("Животное не найдено")
        else:
            if not user.is_authenticated:
                queryset = self.filter_queryset(self.get_queryset())
            else:
                queryset = self.filter_queryset(self.get_queryset().filter(owner_id=user.owner.id))
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response({'beasts': serializer.data})

    @action(methods=['get'], detail=False, permission_classes=[IsAuthenticated])
    def get_my_beasts(self, request, pk=None):
        user = request.user
        query = Beast.objects.filter(owner_id=user.owner.id)
        serializer = self.get_serializer(query, many=True)
        return Response({"beasts": serializer.data}, status=200)


class LaboratoryModelViewSet(ModelViewSet):
    queryset = Laboratory.objects.all()
    serializer_class = LaboratoryModelSerializer
    permission_classes = [IsAuthenticated]

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset().filter(owner_id=request.user.owner.id))
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response({'laboratories': serializer.data})


class CallDoctorModelViewSet(ModelViewSet):
    queryset = CallDoctor.objects.all()
    serializer_class = CallDoctorModelSerializer
    permission_classes = [IsAuthenticated]

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset().filter(user__owner_id=request.user.owner.id))
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response({'call_doctors': serializer.data})


class BeastViewModelViewSet(ModelViewSet):
    queryset = BeastView.objects.all()
    serializer_class = BeastViewModelSerializer
    permission_classes = [IsAuthenticated]

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response({'views': serializer.data})


class InspectionModelViewSet(ModelViewSet):
    queryset = Inspection.objects.all()
    serializer_class = InspectionModelSerializer
    permission_classes = [IsAuthenticated]

    def list(self, request, *args, **kwargs):
        status = request.GET.get('status', None)
        if status:
            queryset = self.filter_queryset(
                self.get_queryset().filter(status=status, user__owner_id=request.user.owner.id))
        else:
            queryset = self.filter_queryset(self.get_queryset().filter(user__owner_id=request.user.owner.id))
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response({'inspections': serializer.data})

    @action(methods=['post'], detail=False)
    def beast_is_delivered(self, request, pk=None):
        inspection = request.data.get('inspection', None)
        if not inspection:
            raise ValidationError('Укажите id')
        try:
            instance = Inspection.objects.get(id=inspection)
            instance.status = 3
            instance.save()
            serializer = self.get_serializer(instance, many=False)
            return Response({'inspection': serializer.data})
        except Inspection.DoesNotExist:
            raise ValidationError("Обьект не найден")

    @action(methods=['post'], detail=False)
    def beast_returned_on_walk(self, request, pk=None):
        inspection = request.data.get('inspection', None)
        if not inspection:
            raise ValidationError('Укажите id')
        try:
            instance = Inspection.objects.get(id=inspection)
            instance.status = 8
            instance.save()
            serializer = self.get_serializer(instance, many=False)
            return Response({'inspection': serializer.data})
        except Inspection.DoesNotExist:
            raise ValidationError("Обьект не найден")

    @action(methods=['post'], detail=False)
    def beast_is_not_delivered(self, request, pk=None):
        inspection = request.data.get('inspection', None)
        if not inspection:
            raise ValidationError('Укажите id')
        try:
            instance = Inspection.objects.get(id=inspection)
            instance.status = 5
            instance.save()
            serializer = self.get_serializer(instance, many=False)
            return Response({'inspection': serializer.data})
        except Inspection.DoesNotExist:
            raise ValidationError("Обьект не найден")

    @action(methods=['post'], detail=False)
    def beast_returned(self, request, pk=None):
        inspection = request.data.get('inspection', None)
        if not inspection:
            raise ValidationError('Укажите id')
        try:
            instance = Inspection.objects.get(id=inspection)
            instance.status = 6
            instance.save()
            serializer = self.get_serializer(instance, many=False)
            return Response({'inspection': serializer.data})
        except Inspection.DoesNotExist:
            raise ValidationError("Обьект не найден")

    @action(methods=['post'], detail=False)
    def beast_is_not_returned(self, request, pk=None):
        inspection = request.data.get('inspection', None)
        if not inspection:
            raise ValidationError('Укажите id')
        try:
            instance = Inspection.objects.get(id=inspection)
            instance.status = 7
            instance.save()
            serializer = self.get_serializer(instance, many=False)
            return Response({'inspection': serializer.data})
        except Inspection.DoesNotExist:
            raise ValidationError("Обьект не найден")

    @action(methods=['post'], detail=False)
    def send_inspection_result(self, request, pk=None):
        inspection = request.data.get('inspection', None)
        health_status = request.data.get('health_status', None)
        comment = request.data.get('comment', None)
        valid_health_status = [0, 1]
        if int(health_status) not in valid_health_status:
            raise ValidationError('Неверно указанный статус здоровья')
        if not inspection:
            raise ValidationError('Укажите id')
        try:
            instance = Inspection.objects.get(id=inspection)
            instance.status = 6
            instance.health_status = health_status
            instance.comment = comment
            instance.save()
            serializer = self.get_serializer(instance, many=False)
            return Response({'inspection': serializer.data})
        except Inspection.DoesNotExist:
            raise ValidationError("Обьект не найден")

    @action(methods=['get'], detail=False)
    def get_veterinarian_event(self, request, pk=None):
        inspections = self.queryset
        status = request.GET.get('status')
        if status:
            inspections = inspections.filter(status=status, user__owner_id=request.user.owner.id)

        serializer = self.get_serializer(inspections, many=True)
        return Response(serializer.data, status=200)

    def create(self, request, *args, **kwargs):
        beasts = request.data.get('beasts', [])
        laboratory = request.data.get('laboratory', None)
        if not beasts:
            raise ValidationError('Укажите животных')
        beasts = Beast.objects.filter(number__in=beasts)
        inspections = []
        for beast in beasts:
            inspections.append(
                Inspection(
                    beast=beast,
                    laboratory_id=laboratory,
                    user_id=request.user.id
                )
            )

        objs = Inspection.objects.bulk_create(inspections)
        serializer = self.get_serializer(objs, many=True)
        return Response({'data': serializer.data})


class DeathModelSerializerModelViewSet(ModelViewSet):
    queryset = Death.objects.all()
    serializer_class = DeathModelSerializer
    permission_classes = [IsAuthenticated]

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset().filter(user__owner_id=request.user.owner.id))
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response({'deathes': serializer.data})

    def create(self, request, *args, **kwargs):
        beasts = request.data.get('beasts', [])
        cause_of_death = request.data.get('cause_of_death', None)
        if not beasts:
            raise ValidationError('Укажите животных')
        beasts = Beast.objects.filter(number__in=beasts)
        deathes = []
        for beast in beasts:
            deathes.append(
                Death(
                    beast=beast,
                    cause_of_death_id=cause_of_death,
                    user_id=request.user.id
                )
            )
            beast.active = False
            beast.save()

        objs = Death.objects.bulk_create(deathes)
        serializer = self.get_serializer(objs, many=True)
        return Response({'data': serializer.data})


class LostModelViewSet(ModelViewSet):
    queryset = Lost.objects.all()
    serializer_class = LostModelSerializer
    permission_classes = [IsAuthenticated]

    @action(methods=['post'], detail=False)
    def beast_is_found(self, request, pk=None):
        lost = request.data.get('lost')
        lost = Lost.objects.get(id=lost)
        lost.status = 1
        lost.save()
        serializer = self.get_serializer(lost, many=False)
        return Response({'lost': serializer.data})

    @action(methods=['post'], detail=False)
    def beast_is_delivered(self, request, pk=None):
        lost = request.data.get('lost')
        lost = Lost.objects.get(id=lost)
        lost.status = 2
        lost.save()
        serializer = self.get_serializer(lost, many=False)
        return Response({'lost': serializer.data})

    @action(methods=['post'], detail=False)
    def beast_is_not_delivered(self, request, pk=None):
        lost = request.data.get('lost')
        lost = Lost.objects.get(id=lost)
        lost.status = 3
        lost.save()
        serializer = self.get_serializer(lost, many=False)
        return Response({'lost': serializer.data})

    def list(self, request, *args, **kwargs):
        status = request.GET.get('status')
        if status:
            queryset = self.filter_queryset(
                self.get_queryset().filter(status=status, user__owner_id=request.user.owner.id))
        else:
            queryset = self.filter_queryset(self.get_queryset().filter(user__owner_id=request.user.owner.id))
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response({'losts': serializer.data})


class CauseDeathModelViewSet(ModelViewSet):
    queryset = CauseDeath.objects.all()
    serializer_class = CauseDeathModelSerializer
    permission_classes = [IsAuthenticated]

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response({'cause_of_deathes': serializer.data})


class HomeView(APIView):
    permission_classes = [AllowAny]

    def get(self, request):
        data = {}
        data['cow_count'] = Beast.objects.count()
        data[
            'cow_action'] = Inspection.objects.all().count() + CallDoctor.objects.all().count() + Lost.objects.all().count() + Death.objects.all().count()
        data['specialist_count'] = User.objects.exclude(user_type='owner').count()
        data['owners'] = User.objects.filter(user_type='owner').count()
        return Response(
            data,
            status=200
        )
