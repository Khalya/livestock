from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from core.models import Beast, BeastView, Laboratory, Inspection, Lost, CallDoctor, Death, CauseDeath
from user.serializers import UserModelSerializer, OwnerModelSerializer


class BeastViewModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = BeastView
        fields = '__all__'


class LaboratoryModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Laboratory
        fields = '__all__'

    def create(self, validated_data):
        user = self.context['request'].user
        validated_data['owner'] = user.owner
        return super(LaboratoryModelSerializer, self).create(validated_data)


class CauseDeathModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = CauseDeath
        fields = '__all__'


class CallDoctorModelSerializer(serializers.ModelSerializer):
    laboratory_data = LaboratoryModelSerializer(read_only=True, required=False, source='laboratory')
    user_data = UserModelSerializer(read_only=True, required=False, source='user')

    def create(self, validated_data):
        user = self.context['request'].user
        validated_data['user'] = user
        return super(CallDoctorModelSerializer, self).create(validated_data)

    class Meta:
        model = CallDoctor
        fields = '__all__'


class BeastModelSerializer(serializers.ModelSerializer):
    beast_view_data = BeastViewModelSerializer(read_only=True, required=False, source='view')
    veterinarian_data = UserModelSerializer(read_only=True, required=False, source='veterinarian')
    owner_data = OwnerModelSerializer(read_only=True, required=False, source='owner')
    imported_data = serializers.SerializerMethodField('get_imported')
    gender_data = serializers.SerializerMethodField('get_gender')
    pedigree_data = serializers.SerializerMethodField('get_pedigree')
    status_data = serializers.SerializerMethodField('get_status')

    def get_status(self, obj):
        return obj.get_status_display()

    def get_imported(self, obj):
        if obj.imported == True:
            return 'Импортное'
        else:
            return 'Местное'

    def get_gender(self, obj):
        return obj.get_gender_display()

    def get_pedigree(self, obj):
        return obj.get_pedigree_display()

    def create(self, validated_data):
        user = self.context['request'].user
        validated_data['veterinarian'] = user
        validated_data['owner'] = user.owner
        return super(BeastModelSerializer, self).create(validated_data)

    class Meta:
        model = Beast
        fields = '__all__'


class InspectionModelSerializer(serializers.ModelSerializer):
    beast_data = BeastModelSerializer(read_only=True, required=False, source='beast')
    laboratory_data = LaboratoryModelSerializer(read_only=True, required=False, source='laboratory')
    beast = serializers.CharField(required=False)
    created = serializers.DateTimeField(format="%d.%m.%Y %H:%M", required=False)
    status_data = serializers.SerializerMethodField('get_status')
    health_status_data = serializers.SerializerMethodField('get_health_status')

    def get_status(self, obj):
        return obj.get_status_display()

    def get_health_status(self, obj):
        return obj.get_health_status_display()

    def create(self, validated_data):
        user = self.context['request'].user
        try:
            beast = Beast.objects.get(number=validated_data.get('beast'))
        except Beast.DoesNotExist:
            raise ValidationError("Животное не найдено")
        validated_data['user'] = user
        validated_data['beast'] = beast
        return super(InspectionModelSerializer, self).create(validated_data)

    class Meta:
        model = Inspection
        fields = '__all__'


class LostModelSerializer(serializers.ModelSerializer):
    beast_data = BeastModelSerializer(read_only=True, required=False, source='beast')
    beast = serializers.CharField(required=False)
    created = serializers.DateTimeField(format="%d.%m.%Y %H:%M", required=False)
    status_data = serializers.SerializerMethodField('get_status')

    def get_status(self, obj):
        return obj.get_status_display()

    def create(self, validated_data):
        user = self.context['request'].user
        try:
            beast = Beast.objects.get(number=validated_data.get('beast'))
        except Beast.DoesNotExist:
            raise ValidationError("Животное не найдено")
        validated_data['user'] = user
        validated_data['beast'] = beast
        return super(LostModelSerializer, self).create(validated_data)

    class Meta:
        model = Lost
        fields = '__all__'


class DeathModelSerializer(serializers.ModelSerializer):
    cause_of_death_data = CauseDeathModelSerializer(read_only=True, required=False, source='cause_of_death')
    created = serializers.DateTimeField(format="%d.%m.%Y %H:%M", required=False)

    def create(self, validated_data):
        user = self.context['request'].user
        validated_data['user'] = user
        return super(DeathModelSerializer, self).create(validated_data)

    class Meta:
        model = Death
        fields = '__all__'
        extra_kwargs = {
            'cause_of_death': {"required": True}
        }
