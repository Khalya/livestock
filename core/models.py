from django.db import models
from user.models import User, Owner


class BeastView(models.Model):
    title = models.CharField(
        verbose_name='Название',
        max_length=255
    )

    class Meta:
        verbose_name = 'Вид животного'
        verbose_name_plural = 'Виды животных'


class Beast(models.Model):
    class GenderChoices(models.IntegerChoices):
        male = 0, 'Самец',
        female = 1, 'Самка'

    class Status(models.IntegerChoices):
        not_imported = 0, 'На месте (никуда не отвозилось)'
        on_place = 1, 'Добралось'
        on_walk = 2, 'В пути'
        not_delivered = 3, 'Не доставлено'
        not_right = 4, 'Отклонено'

    class PedigreeChoices(models.IntegerChoices):
        yes = 1, 'Породное',
        no = 0, 'Беспородное'

    imported = models.BooleanField(
        verbose_name='Животное импортное ?',
        default=False
    )
    number = models.CharField(
        verbose_name='Номер ушной бирки',
        max_length=255,
        help_text="Номер ушной бирки.",
        db_index=True,
        unique=True
    )
    view = models.ForeignKey(
        to=BeastView,
        verbose_name='Вид животного',
        on_delete=models.SET_NULL,
        related_name='beast_views',
        null=True
    )
    gender = models.SmallIntegerField(
        verbose_name='Пол',
        default=0,
        choices=GenderChoices.choices
    )
    birthday_date = models.DateField(
        verbose_name='Дата рождения',
        null=True,
        blank=True
    )
    pedigree = models.SmallIntegerField(
        verbose_name='Породная ?',
        default=0,
        choices=PedigreeChoices.choices,
    )
    foreign_tag = models.CharField(
        verbose_name='Номер иностранной ушной бирки',
        max_length=255,
        null=True,
        blank=True,
        db_index=True,
        unique=True
    )
    status = models.SmallIntegerField(
        verbose_name='Статус',
        choices=Status.choices,
        default=0
    )
    location = models.CharField(
        'Локация',
        max_length=255
    )
    active = models.BooleanField(
        verbose_name='Жив ?',
        default=True
    )
    veterinarian = models.ForeignKey(
        to=User,
        verbose_name='Врач',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='beasts'
    )
    owner = models.ForeignKey(
        Owner,
        verbose_name='Хозяйство',
        null=True,
        on_delete=models.SET_NULL,
        related_name='owner_beasts'
    )

    def __str__(self):
        return str(self.number)

    class Meta:
        verbose_name = 'Животное'
        verbose_name_plural = 'Животные'


class Laboratory(models.Model):
    title = models.CharField(
        verbose_name="Название",
        max_length=255
    )
    owner = models.ForeignKey(
        Owner,
        verbose_name='Хозяйство',
        null=True,
        on_delete=models.SET_NULL,
        related_name='laboratories'
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Лаборатория'
        verbose_name_plural = 'Лаборатории'


class CauseDeath(models.Model):
    title = models.CharField(
        verbose_name="Название",
        max_length=255
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Причина смерти'
        verbose_name_plural = 'Причины смерти'


class Inspection(models.Model):
    class Status(models.IntegerChoices):
        on_walk = 1, 'В пути'
        not_delivered = 2, 'В процессе'
        not_right = 3, 'Осмотрено'
        delivered_to_veterinarian = 4, 'Подтверждение прибытия к врачу'
        not_delivered_to_veterinarian = 5, 'Животное не прибыло к врачу'
        beast_return = 6, 'Животное возвращено'
        beast_not_return = 7, 'Животное не было возвращено'
        beast_return_on_walk = 8, 'Отправлено назад (В пути)'

    class HealthStatus(models.IntegerChoices):
        healthy = 0, 'Животное здорове'
        sick = 1, 'Животное больное'

    beast = models.ForeignKey(
        Beast,
        on_delete=models.SET_NULL,
        null=True,
        verbose_name='Животное'
    )
    laboratory = models.ForeignKey(
        Laboratory,
        on_delete=models.CASCADE,
        null=True,
        verbose_name='Лаборатория'
    )
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        null=True,
        verbose_name='Ветеринар'
    )
    comment = models.TextField(
        verbose_name='Подробнее',
        null=True
    )
    created = models.DateTimeField(
        auto_now_add=True
    )
    status = models.SmallIntegerField(
        choices=Status.choices,
        default=1
    )
    health_status = models.SmallIntegerField(
        choices=HealthStatus.choices,
        default=1
    )

    def save(self, **kwargs):
        super(Inspection, self).save(**kwargs)
        self.beast.status = 2
        self.beast.save()


class CallDoctor(models.Model):
    laboratory = models.ForeignKey(
        Laboratory,
        on_delete=models.CASCADE,
        null=True,
        verbose_name='Лаборатория'
    )
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        null=True,
        verbose_name='Пользователь'
    )
    content = models.TextField(
        verbose_name="Комментарий"
    )
    created = models.DateTimeField(
        auto_now_add=True
    )

    def __str__(self):
        return str(self.laboratory)

    class Meta:
        verbose_name = 'Вызов врача'
        verbose_name_plural = 'Вызовы врача'


class Lost(models.Model):
    class Status(models.IntegerChoices):
        lost = 0, "Животное потеряно"
        found = 1, "Животное найдено в пути"
        delivered = 2, 'Животное доставлено'
        not_delivered = 3, 'Животное не прибыло'

    beast = models.ForeignKey(
        Beast,
        on_delete=models.SET_NULL,
        null=True,
        verbose_name='Животное'
    )
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        null=True,
        verbose_name='Ветеринар'
    )
    comment = models.TextField(
        verbose_name='Комментарий'
    )
    created = models.DateTimeField(
        auto_now_add=True
    )
    status = models.SmallIntegerField(
        choices=Status.choices,
        default=0
    )

    def __str__(self):
        return str(self.beast)

    class Meta:
        verbose_name = 'Выбытие'
        verbose_name_plural = 'Выбытие'


class Death(models.Model):
    beast = models.ForeignKey(
        Beast,
        on_delete=models.SET_NULL,
        null=True,
        verbose_name='Животное'
    )
    cause_of_death = models.ForeignKey(
        CauseDeath,
        on_delete=models.CASCADE,
        null=True,
        verbose_name='Причина смерти'
    )
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        null=True,
        verbose_name='Ветеринар'
    )
    created = models.DateTimeField(
        auto_now_add=True
    )

    def __str__(self):
        return f"{self.beast}"

    class Meta:
        verbose_name = 'Смерть животного'
        verbose_name_plural = 'Смерти животных'
