from django.urls import path
from rest_framework.routers import SimpleRouter

from core.api_view import BeastModelViewSet, InspectionModelViewSet, BeastViewModelViewSet, HomeView, LostModelViewSet, \
    LaboratoryModelViewSet, CallDoctorModelViewSet, CauseDeathModelViewSet, DeathModelSerializerModelViewSet

router = SimpleRouter()
router.register('beasts', BeastModelViewSet)
router.register('inspections', InspectionModelViewSet)
router.register('beast_views', BeastViewModelViewSet)
router.register('lost', LostModelViewSet)
router.register('laboratory', LaboratoryModelViewSet)
router.register('call_doctor', CallDoctorModelViewSet)
router.register('cause_of_deathes', CauseDeathModelViewSet)
router.register('deathes', DeathModelSerializerModelViewSet)

urlpatterns = [
    path('home/', HomeView.as_view(), name='home')
]
